import { Component } from '@angular/core';

import { NavController,NavParams  } from 'ionic-angular';

@Component({
  selector: 'hymn_detail',
  templateUrl: 'hymn_detail.html'
})
export class Hymn_detailPage {

    hymn:any;

  constructor(public navCtrl: NavController, public params:NavParams) {
     this.hymn = params.get('hymn');
  }

}
