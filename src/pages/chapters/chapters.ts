import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import {OldService} from '../../app/services/oldservice';
import {IOld} from "../../interfaces/old.model";
import {Http} from '@angular/http';


@Component({
  selector: 'page-chapters',
  templateUrl: 'chapters.html',
  providers: [OldService]
})
export class ChaptersPage {
    books :any[];
    old:any[];
    hymns:any;
    chapters:string[];
    name:IOld[];
    verses:string[];
    showVerse:boolean = false;
    showChapter:boolean = false;
    selectedBook:string;
    
  constructor(public navCtrl: NavController, private http:Http, private oldservice:OldService ) {

    //  this.oldservice.getOld().subscribe(old => {  console.log(old);  this.books = old,  error => this.handleError(error)});

     
  }


//   ngOnInit():void{
//       let books = [];
//     this.oldservice.getOld().subscribe(data =>{

//       for(let i=0; i < data[0].books.length; i++){
//       books.push(data[0].books[i].name); 
//       }
//       console.log(data[0].books[0].name);
//       data.filter(item=>{ 
//         // console.log(item.books),
//         this.books = books;
//         // (item.books = this.books)
//       })

//       // console.log(data);
//       //  this.books = data;

//     })
//   }


  getChapters(selectedBook:string){
    this.selectedBook = selectedBook;
    let chapters = [];
    this.oldservice.getOld().subscribe(data =>{

      for(let i=0; i < data[0].books.length; i++){
        if(data[0].books[i].name == selectedBook){
            
          for(let j=0; j<data[0].books[i].chapters.length; j++){
          chapters.push(data[0].books[i].chapters[j].name); 

          }

        }
      }
      
      this.chapters = chapters;
      this.showChapter = true;

    })
  }

  getVerses(selectedChapter:string){
    let selectedBook = this.selectedBook;
        let verses = [];
    this.oldservice.getOld().subscribe(data =>{

      for(let i=0; i < data[0].books.length; i++){
 
            
          for(let j=0; j<data[0].books[i].chapters.length; j++){

            if(data[0].books[i].chapters[j].name == selectedChapter
              && data[0].books[i].name == selectedBook)
            {

              for(let k =0; k < data[0].books[i].chapters[j].verses.length; k++ ){
               verses.push(data[0].books[i].chapters[j].verses[k]); 
              }

            }

          }

        
      }
      
      this.verses = verses;
      this.showVerse = true;

    })
  }

  
handleError(error){

  }

  
}
