
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Http} from '@angular/http';
import {HymnService} from '../../app/services/hymnservice';
import { Hymn_detailPage } from '../hymn_detail/hymn_detail';
import {IHymn} from "../../interfaces/hymn.model";
import {ListPage} from "../list/list"

 
 
@Component({
  selector: 'page-hymn',
  templateUrl: 'hymn.html',
  providers: [HymnService]
})
export class HymnPage  {
  req:any;
  hymns:IHymn[];

  constructor(public navCtrl: NavController, private http:Http, private hymnservice:HymnService ) {
     this.hymnservice.getHymns().subscribe(hymn=> this.hymns = hymn , error => this.handleError(error));

  }

  handleError(error){

  }

  


	viewItem(hymn){

	this.navCtrl.push(Hymn_detailPage, {
	hymn:hymn

	});
	}



}

