import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-newtestament',
  templateUrl: 'newtestament.html'
})
export class NewPage {

  constructor(public navCtrl: NavController) {

  }

}
