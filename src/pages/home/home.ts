import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Http} from '@angular/http';
import {HymnService} from '../../app/services/hymnservice';
import {IHymn} from "../../interfaces/hymn.model";
import {ListPage} from "../list/list"

 
 
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [HymnService]
})
export class HomePage  {
  req:any;
  hymns:IHymn[];

  constructor(public navCtrl: NavController, private http:Http, private hymnservice:HymnService ) {
     this.hymnservice.getHymns().subscribe(hymn=> this.hymns = hymn , error => this.handleError(error));

  }

  handleError(error){

  }

  


	viewItem(item){

	this.navCtrl.push(ListPage, {
	item:item

	});
	}



}
