export class IOld{
    books:IBook[];
  
}

export class IBook{
    name:string;
    chapters:IChapters[];
}

export class IChapters{
    name:string;
    verses:string[];
}
