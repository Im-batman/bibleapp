import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";
import 'rxjs/add/operator/catch';
import {IOld} from "../../interfaces/old.model";



@Injectable()
export class  OldService {

    private oldUrl:string = 'assets/old.json';  // URL to web API

    constructor (private http: Http) {}

    getOld(): Observable<IOld[]> {
        return this.http.get(this.oldUrl)
            .map((res:Response)=> <IOld[]>res.json())
            .catch(this.handleError);
    }


    private handleError (error: Response | any) {
        // In a real world app, you might use a remote logging infrastructure

        console.error(JSON.stringify(error));
        return Observable.throw(JSON.stringify(error));
    }


}