import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";
import 'rxjs/add/operator/catch';
import {IHymn} from "../../interfaces/hymn.model";



@Injectable()
export class  HymnService {

    private hymnUrl:string = 'assets/hymn.json';  // URL to web API

    constructor (private http: Http) {}

    getHymns(): Observable<IHymn[]> {
        return this.http.get(this.hymnUrl)
            .map(res=> <IHymn[]>res.json())
            .catch(this.handleError);
    }


    private handleError (error: Response | any) {
        // In a real world app, you might use a remote logging infrastructure

        console.error(JSON.stringify(error));
        return Observable.throw(JSON.stringify(error));
    }


}