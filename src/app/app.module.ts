import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import {HymnPage}   from '../pages/hymn/hymn';
import {OldPage}   from '../pages/oldtestament/oldtestament';
import {NewPage}  from '../pages/newtestament/newtestament';
import { Hymn_detailPage } from '../pages/hymn_detail/hymn_detail';
import { ChaptersPage }   from '../pages/chapters/chapters';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from "@angular/http";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    HymnPage,
    OldPage,
    NewPage,
    Hymn_detailPage,
    ChaptersPage
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    HymnPage,
    OldPage,
    NewPage,
    Hymn_detailPage,
    ChaptersPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
