import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
//import { ListPage } from '../pages/list/list';
import { HymnPage } from '../pages/hymn/hymn';
import { OldPage }  from '../pages/oldtestament/oldtestament';
import {HymnService} from './services/hymnservice';
import { OldService } from './services/oldservice';
import { NewPage } from '../pages/newtestament/newtestament';


@Component({
  templateUrl: 'app.html',
   providers: [HymnService,OldService]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
     // { title: 'List', component: ListPage },
      { title: 'Old Testament',component: OldPage },
      { title: ' New Testament',component: NewPage},
      { title: 'Atsam (NKST Hymns)', component: HymnPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
